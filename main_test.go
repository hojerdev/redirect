package main

import (
	"net/url"
	"testing"
)

func TestMergePathsAndQueryParams(t *testing.T) {
	tests := []struct {
		destination    string
		remainingPath  string
		originalQuery  string
		expectedResult string
	}{
		{
			destination:    "https://example.com/path",
			remainingPath:  "/extra",
			originalQuery:  "foo=bar",
			expectedResult: "https://example.com/path/extra?foo=bar",
		},
		{
			destination:    "https://example.com/path/",
			remainingPath:  "extra",
			originalQuery:  "foo=bar",
			expectedResult: "https://example.com/path/extra?foo=bar",
		},
		{
			destination:    "https://example.com/path",
			remainingPath:  "extra",
			originalQuery:  "foo=bar&baz=qux",
			expectedResult: "https://example.com/path/extra?foo=bar&baz=qux",
		},
		{
			destination:    "https://example.com/path?existing=param",
			remainingPath:  "extra",
			originalQuery:  "foo=bar",
			expectedResult: "https://example.com/path/extra?existing=param&foo=bar",
		},
		{
			destination:    "https://example.com/path",
			remainingPath:  "",
			originalQuery:  "foo=bar",
			expectedResult: "https://example.com/path?foo=bar",
		},
		{
			destination:    "https://example.com/path/",
			remainingPath:  "/extra",
			originalQuery:  "",
			expectedResult: "https://example.com/path/extra",
		},
	}

	for _, tt := range tests {
		result := mergePathsAndQueryParams(tt.destination, tt.remainingPath, tt.originalQuery)
		if result != tt.expectedResult {
			t.Errorf("mergePathsAndQueryParams(%s, %s, %s) = %s; want %s",
				tt.destination, tt.remainingPath, tt.originalQuery, result, tt.expectedResult)
		}
	}
}

func TestFindRedirect(t *testing.T) {
	mappings := map[string]string{
		"/exact":            "https://example.com/exact",
		"/prefix/*":         "https://example.com/prefix/",
		"/assets/*":         "https://example.com/assets/",
		"/query":            "https://example.com/query?existing=param",
		"/another/exact":    "https://example.com/another/exact",
		"/another/prefix/*": "https://example.com/another/prefix/",
	}

	tests := []struct {
		inputPath     string
		expectedURL   string
		expectedFound bool
	}{
		{
			inputPath:     "/exact",
			expectedURL:   "https://example.com/exact",
			expectedFound: true,
		},
		{
			inputPath:     "/prefix/something",
			expectedURL:   "https://example.com/prefix/something",
			expectedFound: true,
		},
		{
			inputPath:     "/assets/img1",
			expectedURL:   "https://example.com/assets/img1",
			expectedFound: true,
		},
		{
			inputPath:     "/query",
			expectedURL:   "https://example.com/query?existing=param",
			expectedFound: true,
		},
		{
			inputPath:     "/notfound",
			expectedURL:   "",
			expectedFound: false,
		},
		{
			inputPath:     "/another/exact",
			expectedURL:   "https://example.com/another/exact",
			expectedFound: true,
		},
		{
			inputPath:     "/another/prefix/extra",
			expectedURL:   "https://example.com/another/prefix/extra",
			expectedFound: true,
		},
	}

	for _, tt := range tests {
		u, _ := url.Parse(tt.inputPath)
		result, found := findRedirect(mappings, u)
		if found != tt.expectedFound || result != tt.expectedURL {
			t.Errorf("findRedirect(%v, %v) = (%v, %v); want (%v, %v)",
				mappings, u, result, found, tt.expectedURL, tt.expectedFound)
		}
	}
}
