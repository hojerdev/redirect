module gitlab.com/hojerst/redirect

go 1.20

require (
	github.com/fsnotify/fsnotify v1.8.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	golang.org/x/sys v0.13.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
