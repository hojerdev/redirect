package main

import (
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync/atomic"
	"time"

	"github.com/fsnotify/fsnotify"
	"gopkg.in/yaml.v3"
)

// Redirects represents the YAML file with the redirect mappings
type Redirects struct {
	Mappings map[string]string `yaml:"mappings"`
}

var atomicRedirectMap atomic.Value

func main() {
	// Load the YAML file with the redirect mappings
	redirectMap := Redirects{}
	err := loadRedirects(&redirectMap)
	if err != nil {
		log.Fatal(err)
	}

	// Set the initial value of atomicRedirectMap
	atomicRedirectMap.Store(redirectMap)

	// Watch the YAML file for changes and reload the mappings if it changes
	go watchRedirects()

	// Define a handler function that redirects to the destination URL
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/health" {
			w.WriteHeader(http.StatusOK)
			w.Write([]byte("OK"))
			return
		}

		if destination, ok := findRedirect(atomicRedirectMap.Load().(Redirects).Mappings, r.URL); ok {
			http.Redirect(w, r, destination, http.StatusFound)
			return
		}

		http.NotFound(w, r)
	})

	// Start the HTTP server on port 8080
	log.Println("Listening on :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// Load the YAML file with the redirect mappings
func loadRedirects(redirectMap *Redirects) error {
	redirectFile, err := os.ReadFile("redirects.yml")
	if err != nil {
		return err
	}
	err = yaml.Unmarshal(redirectFile, redirectMap)
	if err != nil {
		return err
	}
	return nil
}

// findRedirect searches for a matching redirect, including handling "/path/*" patterns, and merges query parameters.
func findRedirect(mappings map[string]string, u *url.URL) (string, bool) {
	// Check for exact match first
	if destination, ok := mappings[u.Path]; ok {
		return mergePathsAndQueryParams(destination, "", u.RawQuery), true
	}

	// Check for "/path/*" patterns
	for key, destination := range mappings {
		if strings.HasSuffix(key, "/*") {
			prefix := strings.TrimSuffix(key, "/*")
			if strings.HasPrefix(u.Path, prefix) {
				remainingPath := strings.TrimPrefix(u.Path, prefix)
				return mergePathsAndQueryParams(destination, remainingPath, u.RawQuery), true
			}
		}
	}

	return "", false
}

// mergePathsAndQueryParams merges the paths of the destination and remainingPath and the original query parameters.
func mergePathsAndQueryParams(destination string, remainingPath string, originalQuery string) string {
	destURL, err := url.Parse(destination)
	if err != nil {
		// If destination URL is invalid, return it as is
		return destination
	}

	// Merge the paths correctly without adding an extra slash
	if remainingPath != "" {
		destURL.Path = strings.TrimRight(destURL.Path, "/") + "/" + strings.TrimLeft(remainingPath, "/")
	} else {
		destURL.Path = strings.TrimRight(destURL.Path, "/")
	}

	// Manually concatenate query parameters
	if originalQuery != "" {
		if destURL.RawQuery != "" {
			destURL.RawQuery += "&" + originalQuery
		} else {
			destURL.RawQuery = originalQuery
		}
	}

	return destURL.String()
}

// Watch the YAML file for changes and reload the mappings if it changes
func watchRedirects() {
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	err = watcher.Add("redirects.yml")
	if err != nil {
		log.Fatal(err)
	}

	for {
		select {
		case event, ok := <-watcher.Events:
			if !ok {
				return
			}
			if event.Op&fsnotify.Write == fsnotify.Write {
				log.Println("Reloading redirects.yml")
				time.Sleep(100 * time.Millisecond)
				redirectMap := Redirects{}
				err := loadRedirects(&redirectMap)
				if err != nil {
					log.Println(err)
				} else {
					atomicRedirectMap.Store(redirectMap)
				}
			}
		case err, ok := <-watcher.Errors:
			if !ok {
				return
			}
			log.Println("error:", err)
		}
	}
}
