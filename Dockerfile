FROM golang:1.24.0@sha256:3f7444391c51a11a039bf0359ee81cc64e663c17d787ad0e637a4de1a3f62a71 AS build

WORKDIR /work

ENV CGO_ENABLED=0

COPY go.mod go.sum *.go ./
RUN go build -o redirect -ldflags '-s'

FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c

COPY --from=build /work/redirect /usr/local/bin/redirect

WORKDIR /app

CMD ["/usr/local/bin/redirect"]
