# Simple HTTP Redirect Service

This is a simple HTTP redirect service that reads redirect mappings from a YAML file and redirects to the destination
URL. The service watches the YAML file for changes and reloads the mappings if it changes.

## Requirements

- Go 1.16 or later
- `github.com/fsnotify/fsnotify` package (`go get github.com/fsnotify/fsnotify`)
- `github.com/go-yaml/yaml` package (`go get github.com/go-yaml/yaml`)

## Installation

Clone this repository and run the following command to install the dependencies:

```shell
go mod download
```

## Usage

To use the service, create a YAML file with the redirect mappings. The YAML file should have the following format:

```yaml
mappings:
  /old-url: https://example.com/new-url
```

Each key-value pair in the mappings field defines a redirect mapping from the old URL to the new URL.

Save the YAML file as redirects.yml in the same directory as the main.go file.

Run the following command to start the service:

```shell
go run main.go
```

The service will start listening on port 8080. When a client requests a URL that matches a redirect mapping in the
redirects.yml file, the service will redirect the client to the corresponding destination URL.

To change the redirect mappings, edit the redirects.yml file and save it. The service will detect the changes and reload
the mappings automatically.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.
